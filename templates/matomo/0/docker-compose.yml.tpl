version: '2'
services:
  matomo:
    image: marvambass/piwik:3.3.0
    volumes:
      - ${config_home}:/var/www/html/config
    #ports:
    #  - "${http_port}:80/tcp"
    #  - "${https_port}:443/tcp"
    environment:
      PIWIK_VERSION: ${PIWIK_VERSION}
      PIWIK_ADMIN: ${PIWIK_ADMIN}
      PIWIK_ADMIN_PASSWORD: ${PIWIK_ADMIN_PASS}
      SITE_NAME: ${SITE_NAME}
      SITE_URL: ${SITE_URL}
      SITE_TIMEZONE: ${timezone}
      #SITE_ECOMMERCE: ${SITE_ECOM}
      #ANONYMISE_IP: ${ANON_IP}
      #DO_NOT_TRACK: ${DNT}
      PIWIK_RELATIVE_URL_ROOT: ${SITE_URL_ROOT}
      #PIWIK_NOT_BEHIND_PROXY: ${BEHIND_PROXY}
      #PIWIK_PLUGINS_ACTIVATE: ${PIWIK_PLUGINS}
      #PIWIK_ADMIN_MAIL: ${PIWIK_ADMIN_EMAIL}
      PIWIK_MYSQL_HOST: "mysql"
      PIWIK_MYSQL_PORT: ${mysql_port}
      PIWIK_MYSQL_DBNAME: ${mysql_dbname}
      PIWIK_MYSQL_USER: ${mysql_user}
      PIWIK_MYSQL_PASSWORD: ${mysql_password}
      PIWIK_MYSQL_PREFIX: ${mysql_prefix}
{{- if ne .Values.db_link ""}}
    external_links:
      - ${db_link}:mysql
{{- else}}
    links:
      - mysql:mysql
  mysql:
    image: marvambass/mysql:latest
    volumes:
      - db_data:/var/lib/mysql
    environment:
      DB_NAME: ${mysql_dbname}
      ADMIN_USER: ${mysql_user}
      ADMIN_PASSWORD: ${mysql_password}
      DB_USER: ${mysql_user}
      DB_PASSWORD: ${mysql_password}
volumes:
  db_data:
    driver: ${volume_driver}
{{- end}}
